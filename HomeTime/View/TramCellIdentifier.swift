//
//  TramCellIdentifier.swift
//  HomeTime
// 
//  Copyright © 2019 REA. All rights reserved.
//

import UIKit

class TramCellIdentifier: UITableViewCell {

    @IBOutlet var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.text = ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
