//
//  Copyright © 2017 REA. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {
    
    //MARK: Outlets
    @IBOutlet var tramTimesTable: UITableView!
    
    //MARK:- Variables
    var northTramsArray:[TramResponseObject] = []
    var southTramsArray:[TramResponseObject] = []
    var isLoadingNorthTrams: Bool = false
    var isLoadingSouthTrams: Bool = false
    
    //MARK:- View Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark) 
    }
    
    //MARK:- Table View Methods
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "North" : "South"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return northTramsArray.count != 0 ? northTramsArray.count : 1
        } else {
            return southTramsArray.count != 0 ? southTramsArray.count : 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TramCellIdentifier", for: indexPath) as! TramCellIdentifier

        if isLoading(section: indexPath.section) {
            cell.titleLabel.text = "Loading upcoming trams..."
        } else {
            cell.titleLabel.text = "No upcoming trams. Tap load to fetch"
            let trams = tramsFor(section: indexPath.section) ?? []
            if (trams.count>indexPath.row){
                let tramResponseObject = trams[indexPath.row]
                let dateString = DotNetDateConverter().formattedDateFromString(tramResponseObject.predictedArrivalDateTime)
                let vehicleNumber = tramResponseObject.vehicleNo
                cell.titleLabel.text = "\(dateString) (Vehicle No.: \(vehicleNumber))" // Added Vehicle No. to make it understandable
            }
        }
        return cell
    }
    
    func isLoading(section: Int) -> Bool {
        return (section == 0) ? isLoadingNorthTrams : isLoadingSouthTrams
    }
    
    func tramsFor(section: Int) -> [TramResponseObject]? {
        return (section == 0) ? northTramsArray : southTramsArray
    }
    
    // MARK: - Button Actions
    @IBAction func clearButtonTapped(_ sender: UIBarButtonItem) {
        clearTramData()
    }
    
    @IBAction func loadButtonTapped(_ sender: UIBarButtonItem) {
        clearTramData()
        initializeTramDataLoadding()
    }
    
    // MARK: - Common Methods
    // ----- Clear All Tram Data Arrays & Reload Table ----- //
    func clearTramData() {
        northTramsArray = []
        southTramsArray = []
        DispatchQueue.main.async {
            self.tramTimesTable.reloadData()
        }
    }
    
     // Initialize tram data fetch by getting 'token' and calling tram fetch method (loadTramDataWith)
    func initializeTramDataLoadding() {
        SVProgressHUD.show()
        let savedApiToken = DefaultWrapper().getSessionToken()
        if (savedApiToken.count > 0) {
            loadTramDataWith(token: savedApiToken)
        }else {
            getApiTokenAndFetchTramData()
        }
    }
    
    //MARK:- Fetch Data From APIManager
    func getApiTokenAndFetchTramData() {
        SVProgressHUD.show()
        ApiManager().fetchApiToken(vc: self, completion: { (fetchedToken, error) in
            if (error == nil) {
                if (fetchedToken.count > 0) {
                    self.loadTramDataWith(token: fetchedToken)
                }
            }
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
            }
        })
    }
    
    // ----- Fetch tram data from web service using the token ----- //
    func loadTramDataWith(token: String) {
        self.isLoadingNorthTrams = true
        self.isLoadingSouthTrams = true
        ApiManager().fetchTramDataFor(vc: self, direction: Direction.north, token: token) { (tramResponseObject, error) in
            if (error == nil) {
                self.northTramsArray = tramResponseObject
            }
            self.isLoadingNorthTrams = false
            self.dismissLoaderIfFetchFinished()
        }
        
        ApiManager().fetchTramDataFor(vc: self, direction: Direction.south, token: token) { (tramResponseObject, error) in
            if (error == nil) {
                self.southTramsArray = tramResponseObject
            }
            self.isLoadingSouthTrams = false
            self.dismissLoaderIfFetchFinished()
        }
    }
    
    func dismissLoaderIfFetchFinished() {
        if (isLoadingNorthTrams == false && isLoadingSouthTrams == false) {
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.tramTimesTable.reloadData()
            }
        }
    }
    
}
