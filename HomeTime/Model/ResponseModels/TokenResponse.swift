//
//  TokenResponse.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import UIKit

class TokenResponse: NSObject {
    var errorMessage:String = ""
    var hasError:Bool = false
    var hasResponse:Bool = false
    var responseObject:[TokenResponseObject] = []
    var timeRequested:String = ""
    var timeResponded:String = ""
    var webMethodCalled:String = ""
    
    init(_ dictionary: [String: Any]) {
        self.errorMessage       = dictionary["errorMessage"] as? String ?? ""
        self.hasError           = dictionary["hasError"] as? Bool ?? false
        self.hasResponse        = dictionary["hasResponse"] as? Bool ?? false
        
        self.responseObject = []
        let responseObjectArray = dictionary["responseObject"] as? [[String:Any]] ?? []
        for i in 0..<responseObjectArray.count {
            let objectAtIndex = TokenResponseObject(responseObjectArray[i])
            self.responseObject.append(objectAtIndex)
        }
        
        self.timeRequested      = dictionary["timeRequested"] as? String ?? ""
        self.timeResponded      = dictionary["timeResponded"] as? String ?? ""
        self.webMethodCalled    = dictionary["webMethodCalled"] as? String ?? ""
    }
}

class TokenResponseObject: NSObject {
    var type:String = ""
    var deviceToken:String = ""
    
    init(_ dictionary: [String: Any]) {
        self.type = dictionary["__type"] as? String ?? ""
        self.deviceToken = dictionary["DeviceToken"] as? String ?? ""
    }
}
