//
//  TramResponse.swift
//  HomeTime
// 
//  Copyright © 2019 REA. All rights reserved.
//

import UIKit

class TramResponse: NSObject {
    var errorMessage:String = ""
    var hasError:Bool = false
    var hasResponse:Bool = false
    var responseObject:[TramResponseObject] = []
    var timeRequested:String = ""
    var timeResponded:String = ""
    var webMethodCalled:String = ""
    
    init(_ dictionary: [String: Any]) {
        self.errorMessage       = dictionary["errorMessage"] as? String ?? ""
        self.hasError           = dictionary["hasError"] as? Bool ?? false
        self.hasResponse        = dictionary["hasResponse"] as? Bool ?? false
        
        self.responseObject = []
        let responseObjectArray = dictionary["responseObject"] as? [[String:Any]] ?? []
        for i in 0..<responseObjectArray.count {
            let objectAtIndex = TramResponseObject(responseObjectArray[i])
            self.responseObject.append(objectAtIndex)
        }
        
        self.timeRequested      = dictionary["timeRequested"] as? String ?? ""
        self.timeResponded      = dictionary["timeResponded"] as? String ?? ""
        self.webMethodCalled    = dictionary["webMethodCalled"] as? String ?? ""
    }
}

class TramResponseObject: NSObject {
    var type:String = ""
    var airConditioned:Bool = false
    var destination:String = ""
    var displayAC:Bool = false
    var disruptionMessage:TramDisruptionMessageObject = TramDisruptionMessageObject([:])
    var hasDisruption:Bool = false
    var hasSpecialEvent:Bool = false
    var headBoardRouteNo:String = ""
    var internalRouteNo:Int = 0
    var isLowFloorTram:Bool = false
    var isTTAvailable:Bool = false
    var predictedArrivalDateTime:String = ""
    var routeNo:String = ""
    var specialEventMessage:String = ""
    var tripID:Int = 0
    var vehicleNo:Int = 0
    
    init(_ dictionary: [String: Any]) {
        self.type               = dictionary["__type"] as? String ?? ""
        self.airConditioned     = dictionary["AirConditioned"] as? Bool ?? false
        self.destination        = dictionary["Destination"] as? String ?? ""
        self.displayAC          = dictionary["DisplayAC"] as? Bool ?? false
        self.disruptionMessage  = TramDisruptionMessageObject(dictionary["responseObject"] as? [String:Any] ?? [:])
        self.hasDisruption     = dictionary["HasDisruption"] as? Bool ?? false
        self.hasSpecialEvent     = dictionary["HasSpecialEvent"] as? Bool ?? false
        self.headBoardRouteNo    = dictionary["HeadBoardRouteNo"] as? String ?? ""
        self.internalRouteNo   = dictionary["InternalRouteNo"] as? Int ?? 0
        self.isLowFloorTram     = dictionary["IsLowFloorTram"] as? Bool ?? false
        self.isTTAvailable     = dictionary["IsTTAvailable"] as? Bool ?? false
        self.predictedArrivalDateTime    = dictionary["PredictedArrivalDateTime"] as? String ?? ""
        self.routeNo    = dictionary["RouteNo"] as? String ?? ""
        self.specialEventMessage    = dictionary["SpecialEventMessage"] as? String ?? ""
        self.tripID   = dictionary["TripID"] as? Int ?? 0
        self.vehicleNo   = dictionary["VehicleNo"] as? Int ?? 0
    }
}


class TramDisruptionMessageObject: NSObject {
    var displayType:String = ""
    var messageCount:Int = 0
    var messages = [] as Array
    
    init(_ dictionary: [String: Any]) {
        self.displayType    = dictionary["DisplayType"] as? String ?? ""
        self.messageCount   = dictionary["MessageCount"] as? Int ?? 0
        self.messages   = dictionary["Messages"] as? Array ?? []
    }
}
