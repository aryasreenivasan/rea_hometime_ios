//
//  ExtensionManager.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import UIKit

extension UIViewController { 
    //MARK:- Single Button Alerts from VC
    func displaySingleButtonErrorAlert(message:String?) {
        DispatchQueue.main.async {
            let messageString = message ?? "Network error occurred"
            let alert = UIAlertController(title: "", message: messageString, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
}
