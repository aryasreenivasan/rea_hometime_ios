//
//  DefaultWrapper.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import UIKit

class DefaultWrapper: NSObject {
    //MARK:- Save & Retricve API Token
    func getSessionToken() -> String {
        return UserDefaults.standard.string(forKey: "SESSION_TOKEN") ?? ""
    }
    
    func setTokenToDefaults(tokenString:String) {
        UserDefaults.standard.set(tokenString, forKey: "SESSION_TOKEN")
    }
}
