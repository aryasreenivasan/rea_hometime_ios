//
//  ApiManager.swift
//  HomeTime
//
//  Copyright © 2019 REA. All rights reserved.
//

import UIKit

enum Direction: String {
    case north
    case south
}

class ApiManager: NSObject {
    
    // MARK: - API Endpoint URLs
    let urlHeader       = "http://ws3.tramtracker.com.au/TramTracker/RestService/"
    
    var tokenUrl        = "GetDeviceToken/?aid=TTIOSJSON&devInfo=HomeTimeiOS"
    let northTransUrl   = "GetNextPredictedRoutesCollection/4055/78/false/?aid=TTIOSJSON&cid=2"
    let southTransUrl   = "GetNextPredictedRoutesCollection/4155/78/false/?aid=TTIOSJSON&cid=2"
    
    // MARK: - Fetch Token
    func fetchApiToken(vc:UIViewController, completion: @escaping (_ token:String, _ error:Error?) -> ()) {
        let requestURLString = "\(urlHeader)\(tokenUrl)"
        let request = NSMutableURLRequest(url: NSURL(string: requestURLString)! as URL)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                completion("",error)
            } else {
                do {
                    if  let jsonDict = try JSONSerialization.jsonObject(with: data!) as? [String:Any] {
                        let tokenResponse = TokenResponse(jsonDict)
                        if (tokenResponse.hasResponse == true && tokenResponse.responseObject.count > 0) {
                            if (tokenResponse.responseObject[0].deviceToken.count > 0) {
                                let fetchedToken = tokenResponse.responseObject[0].deviceToken
                                // ----- Saved the token to Defaults for future API calls (If there is no expiry for the token). Otherwise, we've to use the local variable, instead of the UserDefaults/call token API again on token expiry ----- //
                                DefaultWrapper().setTokenToDefaults(tokenString: fetchedToken)
                                completion(fetchedToken,nil)
                            }else {
                                vc.displaySingleButtonErrorAlert(message: "Server error occurred") // Empty responseObject
                                completion("",nil)
                            }
                        }else {
                            vc.displaySingleButtonErrorAlert(message: "Server error occurred.") // Empty/no token
                            completion("",nil)
                        }
                    }else {
                        vc.displaySingleButtonErrorAlert(message: "Server error occurred...") // Invalid token
                        completion("",nil)
                    }
                } catch let parsingError {
                    vc.displaySingleButtonErrorAlert(message: parsingError.localizedDescription)
                     completion("",parsingError)
                }
            }
        })
        dataTask.resume()
    }
    
    // MARK: - Fetah Tram Data
    func fetchTramDataFor(vc:UIViewController, direction:Direction, token:String, completion: @escaping ([TramResponseObject], _ error:Error?) -> ()) {
        var requestURLString = "\(urlHeader)"
        requestURLString = (direction == Direction.north) ? "\(urlHeader)\(northTransUrl)&tkn=\(token)" : "\(urlHeader)\(southTransUrl)&tkn=\(token)"
        
        let request = NSMutableURLRequest(url: NSURL(string: requestURLString)! as URL)
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print(error ?? "")
                completion([],error)
            } else {
                do {
                    if  let jsonDict = try JSONSerialization.jsonObject(with: data!) as? [String:Any] {
                        let tramResponseObject = TramResponse(jsonDict)
                        if (tramResponseObject.hasResponse == true) {
                            completion(tramResponseObject.responseObject,nil)
                        }else {
                            vc.displaySingleButtonErrorAlert(message: "Server error occurred") // hasResponse == false
                            completion([],nil)
                        }
                    }else {
                        completion([],nil)
                    }
                } catch let parsingError {
                    completion([],parsingError)
                }
            }
        })
        dataTask.resume()
    }
}
